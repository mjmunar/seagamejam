﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCutScene : MonoBehaviour
{
    [SerializeField] private GameObject m_bg;
    [SerializeField] private GameObject m_player1;
    [SerializeField] private GameObject m_player2;
    [SerializeField] private GameObject m_mainLobby;

    public void ShowFinalMenu()
    {
        m_bg.SetActive(false);
        m_player1.SetActive(false);
        m_player2.SetActive(false);
        
        m_mainLobby.SetActive(true);
    }
}
