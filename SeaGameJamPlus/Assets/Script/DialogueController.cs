﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using static AstroType;

[Serializable]
public class DialogueItem
{
    public string[] Message;
    public AstroType[] Name;

    private int counter = -1;
    
    public string GetMessage()
    {
        counter++;

        if (counter >= Message.Length)
        {
            counter = 0;
        }
        else if(counter < 0)
        {
            counter = 0;
        }

        return Message[counter];
    }

    public AstroType GetAstroType()
    {
        return Name[counter];
    }
}

public enum AstroType
{
    Legit,
    Steam,
    Captain
}

public class DialogueController : MonoBehaviour
{
    public static DialogueController Instance;
    [SerializeField] private TextMeshProUGUI m_dialogue;
    [SerializeField] private UnityEngine.UI.Image m_legitAstro;
    [SerializeField] private UnityEngine.UI.Image m_steamAstro;
    [SerializeField] private DialogueItem[] m_dialogueMessage;
    [SerializeField] private GameObject m_dialogueCanvas;

    [SerializeField] private Animator m_endCutScene;
    
    [SerializeField] private GameObject m_endCutSceneObj;
    
    
    public string story;
    public bool m_donePlayingText = true;

    public int m_currentMessageIndex;
    public int m_currentDialogueIndex;
    private bool m_doneMessage;

    private void Awake()
    {
        Instance = this;
       // DisplayDialogue();
    }

    public void PlayEndCutScene()
    {
        
    }

    public void DisplayDialogue()
    {
        m_donePlayingText = true;
        story = "";
        DisplayMessageText();
        m_dialogueCanvas.SetActive(true);
        
    }

    private void DisplayMessageText()
    {
        if (m_doneMessage)
        {
            m_donePlayingText = true;
            m_currentMessageIndex = 0;
            StopCoroutine(PlayText());
            CloseDialogue();
        }
        else
        {
            if (m_donePlayingText)
            {
                m_donePlayingText = false;
                story = m_dialogueMessage[m_currentDialogueIndex].GetMessage();
                
                
                
                switch (m_dialogueMessage[m_currentDialogueIndex].GetAstroType())
                {
                    case Legit:
                        m_dialogue.alignment = TextAlignmentOptions.TopLeft;
                        m_legitAstro.color = Color.white;
                        m_steamAstro.color = Color.gray;
                        break;
                    case Steam:
                        m_dialogue.alignment = TextAlignmentOptions.TopRight;
                        m_legitAstro.color = Color.gray;
                        m_steamAstro.color = Color.white;
                        break;
                    case Captain:
                        m_dialogue.alignment = TextAlignmentOptions.TopLeft;
                        m_legitAstro.color = Color.gray;
                        m_steamAstro.color = Color.gray;

                        story = "Captain: " + story;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                m_currentMessageIndex++;
                
                m_dialogue.text = "";
                StartCoroutine(PlayText());
            }
            else
            {
                Debug.Log("FULL DONE");
                m_donePlayingText = true;
                StopCoroutine(PlayText());
                m_dialogue.text = story;
                
                if (m_currentMessageIndex >= m_dialogueMessage[m_currentDialogueIndex].Message.Length)
                {
                    m_doneMessage = true;
                }
            }
        }
    }

    private void Update () 
    {
        if (Input.GetMouseButtonDown(0) && m_dialogueCanvas.activeInHierarchy)
        {
            DisplayMessageText();
        }
    }

    private IEnumerator PlayText()
    {
        int counter = 0;
        
        foreach (char c in story)
        {
            if (!m_donePlayingText)
            {
                counter++;
                m_dialogue.text += c;

                if (counter >= story.Length)
                {
                    m_donePlayingText = true;
                    StopCoroutine(PlayText());
                    
                    if (m_currentMessageIndex >= m_dialogueMessage[m_currentDialogueIndex].Message.Length)
                    {
                        m_doneMessage = true;
                    }
                }

                yield return new WaitForSeconds(0.05f);
            }
        }
    }

    private void CloseDialogue()
    {
        m_doneMessage = false;
        m_donePlayingText = true;
        m_currentMessageIndex = 0;
        m_dialogueCanvas.SetActive(false);

        m_currentDialogueIndex++;
        
        if (m_currentDialogueIndex == 7)
        {
            m_endCutScene.enabled = true;
        }
    }

    public bool IsOpen()
    {
        return m_dialogueCanvas.activeInHierarchy;
    }

    public void OpenEndScene()
    {
        m_endCutSceneObj.SetActive(true);
    }
   
    
    
}
