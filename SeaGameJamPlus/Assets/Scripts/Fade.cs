﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    public static Fade Instance;
    public SpriteRenderer m_spriteRenderer;

    /// <summary>
    /// The duration to fade in.
    /// </summary>
    public float fadeInDuration = 0.5f;

    /// <summary>
    /// The duration to stay visible before fading out.
    /// </summary>
    public float duration = 1f;

    /// <summary>
    /// The duration to fade out. If zero, the control doesn't fade out.
    /// </summary>
    public float fadeOutDuration = 0.5f;
    

    private void Awake()
    {
        Instance = this;

     // StartCoroutine(IFadeOut());
       // StartCoroutine(IFadeIn());
    }
    
    public void FadeIn(float fadeInDuration, float duration, SpriteRenderer spriteRenderer)
    {
        this.fadeInDuration = fadeInDuration;
        this.duration = duration;
        this.m_spriteRenderer = spriteRenderer;
        
        StartCoroutine(IFadeIn());
    }
    
    
    public void FadeOut(float duration, float fadeOutDuration, SpriteRenderer spriteRenderer)
    {
        this.duration = duration;
        this.fadeOutDuration = fadeOutDuration;
        this.m_spriteRenderer = spriteRenderer;
        
        StartCoroutine(IFadeOut());
    }
    
    /// <summary>
    /// Plays the fade effect.
    /// </summary>
    public IEnumerator IFadeIn()
    {
        // Fading animation
        float start = Time.time;
        while (Time.time <= start + duration)
        {
            Color color = m_spriteRenderer.color;
            color.a = 0 + Mathf.Clamp01((Time.time - start) / duration);
            m_spriteRenderer.color = color;
            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary>
    /// Plays the fade effect.
    /// </summary>
    public IEnumerator IFadeOut()
    {
        Debug.LogError("tinwag");
        // Fading animation
        float start = Time.time;
        while (Time.time <= start + duration)
        {
            Color color = m_spriteRenderer.color;
            color.a = 1f - Mathf.Clamp01((Time.time - start) / duration);
            m_spriteRenderer.color = color;
            yield return new WaitForEndOfFrame();
        }
    }
    
  
    
}
