﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockerObject : MonoBehaviour
{
    // Start is called before the first frame update

   
  

    [SerializeField] private bool m_stopMoving = false;

    [SerializeField] private float m_rightLimit;
    [SerializeField] private float m_leftLimit;
    
   
    [SerializeField] private float m_upLimit;
    [SerializeField] private float m_downLimit;
    [SerializeField] private float m_speed;
    private int direction = 1;


    [SerializeField] private bool m_isVertical = false;
 
    void FixedUpdate() {

        if (!m_stopMoving)
        {
            Movement();
        }
    }

    public void StartMoving()
    {
        m_stopMoving = false;
    }
    
    public void StopMoving()
    {
        m_stopMoving = true;
    }
    
    

    void Movement()
    {
        Vector2 movement;

        if (!m_isVertical)
        {
            if (transform.position.x > m_rightLimit)
            {
                direction = -1;
            }
            else if (transform.position.x < m_leftLimit)
            {
                direction = 1;
            }
            
            movement = Vector2.right * direction * m_speed * Time.deltaTime; 
        }
        else
        {
            if (transform.position.y > m_upLimit)
            {
                direction = -1;
            }
            else if (transform.position.y < m_downLimit)
            {
                direction = 1;
            }
            
            movement = Vector2.up * direction * m_speed * Time.deltaTime; 
        }

       
         
        transform.Translate(movement); 
    }
    
   
}
