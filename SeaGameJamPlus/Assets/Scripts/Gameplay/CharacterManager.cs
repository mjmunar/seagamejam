﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager Instance;

    [SerializeField] private GameObject m_player1;
    [SerializeField] private GameObject m_player2;
    [SerializeField] private CharacterController m_player1Controller;
    [SerializeField] private CharacterController m_player2Controller;
    
    [SerializeField] private Collider2D m_player1Collider;
    [SerializeField] private Collider2D m_player2Collider;

    [SerializeField] private GameObject m_lineRenderer;
    
    
    [SerializeField] private bool isPlayer1Moving = false;
    [SerializeField] private bool isPlayer2Moving = false;
    public float maxDist = 8;
    public float maxMidPoint = 6;
    public float midDist = 4;
    public float minMidPoint = 2;
    public float minDist = 1;
    

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        CheckDistance(GetPlayerDistance());
    }

    public void TogglePlayerStatus(string p_tag, bool p_status)
    {
        if(p_tag.Equals("Player1"))
        {
            isPlayer1Moving = p_status;
        }
        else
        {
            isPlayer2Moving = p_status;
        }
    }

    public float GetPlayerDistance()
    {
        return Vector3.Distance(m_player1.transform.position, m_player2.transform.position);
    }

    private void CheckDistance(float p_distance)
    {
        if (p_distance > maxDist)
        {
            if (!GameLoader.Instance.IsGameLoaderResetting())
            {
                
                m_player1Controller.StopMovement();
                m_player2Controller.StopMovement();
                LineController.Instance.EndDrawLine();
             
                GameLoader.Instance.ResetLevel(0);
            }

        }
        else if (p_distance < minDist)
        {
             Debug.Log("Social Distancing pls");
        }
    }
    
    public string GetCurrentMovingPlayer()
    {
        return isPlayer1Moving ? "Player1" : "Player2";
    }

    public Vector2 GetCurrentPlayerMovingPosition()
    {
        return isPlayer1Moving ? m_player1Controller.GetPlayerPosition() : m_player2Controller.GetPlayerPosition();
    }
    
    public void MoveCurrentPlayer(Vector2 p_targetPos)
    {
        if (isPlayer1Moving)
        {
            m_player1Controller.InitializeMoveTowardsNode(p_targetPos);
        }
        else
        {
            m_player2Controller.InitializeMoveTowardsNode(p_targetPos);
        }
    }

    public GameObject GetPlayer1()
    {
        return m_player1;
    }

    public GameObject GetPlayer2()
    {
        return m_player2;
    }

    public CharacterController GetPlayerController(string p_value)
    {
        if (p_value == "Player1")
        {
            return m_player1Controller;
        }
        else
        {
            return m_player2Controller;
        }
       
    }

    public void ResetPlayerData()
    {
        m_player1Controller.ResetPlayer();
        m_player2Controller.ResetPlayer();
    }

    public void SetObjectsActive(bool p_value)
    {
        if (DialogueController.Instance != null)
        {
            if (!DialogueController.Instance.isActiveAndEnabled)
            {
                m_player1Collider.enabled = p_value;
                m_player2Collider.enabled = p_value;
            }
        }

        m_lineRenderer.SetActive(p_value);
        m_player1.SetActive(p_value);
        m_player2.SetActive(p_value);
    }

    public void SetCollider(bool p_value)
    {
        m_player1Collider.enabled = p_value;
        m_player2Collider.enabled = p_value;
    }
    
    
    
    
    
    
    
}
