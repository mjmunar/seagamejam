﻿using System;
using System.Collections;
using System.Diagnostics;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class CharacterController : MonoBehaviour
{
	[SerializeField] private float m_speed;
	[SerializeField] private Rigidbody2D m_rigidBody2D;
	[SerializeField] private Vector2 m_minPower;
	[SerializeField] private Vector2 m_maxPower;
	[SerializeField] private LineRenderer m_lineRenderer;
	[SerializeField] private string m_tag;

	private Vector2 m_ballForce;
	private Vector3 m_startPoint;
	private Vector3 m_endPoint;

	private Camera m_camera;

	[SerializeField] private bool isFlying = false;
	private bool isClickingPlayer = false;

	[SerializeField] private bool m_movePlayer = false;

	[SerializeField] private Animator m_animator;
	
	[SerializeField] GameObject  m_particle;

	private Vector2 m_targetPosition;

	[SerializeField] private float timer;

	private bool m_mainMenuCollided = false;

	private Vector2 m_mainMenuStartPos;
	
	[SerializeField] private float m_flyingTimerLimit;
 
	private void Awake()
	{
		m_camera = Camera.main;
		m_mainMenuStartPos = transform.position;
	}


	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.CompareTag("MainMenuNode") )
		{
			InitializeMoveTowardsNode(col.transform.position);
			StartCoroutine(StartNextLevel());
		
			

		}
	}

	IEnumerator StartNextLevel()
	{
	
		yield return new WaitForSeconds(1);
		
		SceneManager.LoadScene(1);
		SoundManager.Instance.PlayBGM(1);
	}

	private void Update()
	{
		if (DialogueController.Instance != null)
		{
			if(DialogueController.Instance.IsOpen())
			{
				return;
			}
		}
		
			if (Input.GetMouseButtonDown(0) && !isFlying)
			{
				m_startPoint = m_camera.ScreenToWorldPoint(Input.mousePosition);
				m_startPoint.z = 15;


				Vector2 ray = m_camera.ScreenToWorldPoint(Input.mousePosition);
				RaycastHit2D rayHit = Physics2D.Raycast(ray, Vector2.zero);
				if (rayHit)
				{
					if (rayHit.collider.CompareTag(m_tag))
					{
						isClickingPlayer = true;
					}
				}
			}


			if (Input.GetMouseButton(0) && isClickingPlayer && !isFlying)
			{
				Vector3 m_currentPoint = m_camera.ScreenToWorldPoint(Input.mousePosition);
				m_currentPoint.z = 15;
				DrawLine(m_startPoint, m_currentPoint);
			}

			if (Input.GetMouseButtonUp(0) && isClickingPlayer && !isFlying)
			{
				m_endPoint = m_camera.ScreenToWorldPoint(Input.mousePosition);
				m_endPoint.z = 15;

				m_ballForce = new Vector2(Mathf.Clamp(m_startPoint.x - m_endPoint.x, m_minPower.x, m_maxPower.x),
					Mathf.Clamp(m_startPoint.y - m_endPoint.y, m_minPower.y, m_maxPower.y));

				if (m_ballForce.x > 0)
				{
					//transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
					transform.localScale = new Vector2(-1, transform.localScale.y);

				}
				else if (m_ballForce.x < 0)
				{

					transform.localScale = new Vector2(1, transform.localScale.y);
				}




				m_rigidBody2D.AddForce(m_ballForce * m_speed, ForceMode2D.Impulse);
				if (CharacterManager.Instance != null)
				{
					CharacterManager.Instance.TogglePlayerStatus(tag, true);
				}

				isClickingPlayer = false;
				SetFlyingStatus(true);
				m_animator.SetBool("IsFlying", true);
				m_particle.SetActive(true);
				EndDrawLine();
			}

			m_rigidBody2D.velocity = Vector2.ClampMagnitude(m_rigidBody2D.velocity, 10);
		



	}

	void FixedUpdate()
	{

		
			if (m_movePlayer)
			{
				if (Vector2.Distance(m_targetPosition, m_rigidBody2D.position) <= 0.1f)
				{
					m_rigidBody2D.velocity = Vector2.zero;
					m_movePlayer = false;
					m_targetPosition = Vector2.zero;
					if (CharacterManager.Instance != null)
					{
						CharacterManager.Instance.TogglePlayerStatus(tag, false);
					}

					SetFlyingStatus(false);
					m_animator.SetBool("IsFlying", false);
					m_particle.SetActive(false);
				}
			}

			if (isFlying)
			{
				timer += Time.deltaTime;
				if (timer >= m_flyingTimerLimit)
				{
					timer = 0;
					if (GameLoader.Instance != null)
					{
						GameLoader.Instance.ResetLevel();
					}
					else
					{
						ResetPlayer();
						transform.position = m_mainMenuStartPos;
						m_animator.SetBool("IsFlying", false);
						
					}
				}
				
			}
			else
			{
				timer = 0;
			}

		






	}

	
	private void DrawLine(Vector3 p_startPoint, Vector3 p_currentPoint)
	{
		m_lineRenderer.positionCount = 2;
		
		Vector3[] points = new Vector3[2];

		points[0] = p_startPoint;
		points[1] = p_currentPoint;
		

		m_lineRenderer.SetPositions(points);

		m_lineRenderer.sortingOrder = 10;
	}

	private void EndDrawLine()
	{
		m_lineRenderer.positionCount = 0;
	}


	void MoveTowardsNode()
	{
		Vector2 dir = m_targetPosition - m_rigidBody2D.position;
		// Get the velocity required to reach the target in the next frame
		dir /= Time.fixedDeltaTime;
		// Clamp that to the max speed
		dir = Vector3.ClampMagnitude(dir, 5);
		// Apply that to the rigidbody
		m_rigidBody2D.velocity = dir;
		
	}

	public Vector2 GetPlayerPosition()
	{
		return m_rigidBody2D.position;
	}


	public void InitializeMoveTowardsNode(Vector2 p_targetPos)
	{
		m_rigidBody2D.velocity = Vector2.zero;
		m_targetPosition = p_targetPos;
		MoveTowardsNode();
		m_movePlayer = true;
		m_animator.SetBool("IsFlying", true);
		m_particle.SetActive(true);
	
	}

	public void SetFlyingStatus(bool p_status)
	{
		isFlying = p_status;
	}

	public void ResetPlayer()
	{
		m_rigidBody2D.velocity = Vector2.zero;
		m_movePlayer = false;
		m_targetPosition = Vector2.zero;
		SetFlyingStatus(false);
		m_particle.SetActive(false);
	}

	public void StopMovement()
	{
		m_rigidBody2D.velocity = Vector2.zero;
		m_particle.SetActive(false);
	}



	
	
	
	
	
}