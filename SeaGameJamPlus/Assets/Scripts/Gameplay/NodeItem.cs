﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeItem : MonoBehaviour
{

    [SerializeField] private Collider2D m_collider;
    [SerializeField] private bool m_endNode = false;
    
    [SerializeField] private bool m_isOccupied = false;

    [SerializeField] private string m_story;
    
    [SerializeField] private bool m_disappering = false;
    
    [SerializeField] private bool m_startNodePlayer1 = false;
    
    [SerializeField] private bool m_startNodePlayer2 = false;
    
    
    [SerializeField] private bool m_isSwitchMovingPlatform = false;
    [SerializeField] private bool m_isSwitchDoor = false;

    private string m_currentOccupiedTag = "";
    
    
    
    public void SetCollider(bool p_value)
    {
        m_collider.enabled = p_value;
    }

    public void SetActiveGameObject(bool p_value)
    {
        this.gameObject.SetActive(p_value);
    }

    public Vector2 GetPosition()
    {
        return transform.position;
    }

    public bool CheckIfTwoPlayersAllowed()
    {
        return m_endNode;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player1") || col.CompareTag("Player2"))
        {
            Debug.Log("continiues collide");
           
                if (!m_isOccupied || m_endNode)
                {
                    m_currentOccupiedTag = col.tag;
                    CharacterManager.Instance.GetPlayerController(col.tag)
                        .InitializeMoveTowardsNode(transform.position);
                    m_isOccupied = true;

                }

                if (m_endNode)
                {
                    GameLoader.Instance.AddGoalCounter();
                }

                if (m_isSwitchMovingPlatform)
                {
                    GameLoader.Instance.StopMovingPlatform();
                }
                
                if (m_isSwitchDoor)
                {
                    GameLoader.Instance.OpenDoor(false);
                }
            
        }
    }

    // IEnumerator CheckIfNoPlayer()
    // {
    //     Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, 0.5f);
    //     
    //     yield return  new WaitForSeconds(2);
    //     Debug.Log("check player");
    //     
    //   
    //     for (int i = 0; i < collider.Length; i++)
    //     {
    //         if (!collider[i].CompareTag("Player1") || !collider[i].CompareTag("Player2"))
    //         {
    //             Debug.Log("hindi player");
    //         }
    //         else
    //         {
    //             Debug.Log("ano to");
    //             m_isOccupied = false;
    //             
    //             if (m_disappering)
    //             {
    //                 SetActiveGameObject(false);
    //             }
    //
    //             if (m_isSwitchMovingPlatform)
    //             {
    //                 GameLoader.Instance.StartOrStopMovingPlatform(true);
    //             }
    //         }
    //     }
    //     
    // }
   
    
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Player1") || col.CompareTag("Player2"))
        {
            // if (Vector2.Distance(transform.position,
            //         CharacterManager.Instance.GetPlayerController("Player1").transform.position) >= 3 &&
            //     Vector2.Distance(transform.position,
            //         CharacterManager.Instance.GetPlayerController("Player2").transform.position) >= 3)
            // {
            //     Debug.Log("kaboom");

            //  }
          //  CheckIfNoPlayer();
           // StartCoroutine("CheckIfNoPlayer");
           if (m_currentOccupiedTag == col.tag)
           {
               m_isOccupied = false;
               if (m_disappering)
               {
                   SetActiveGameObject(false);
               }
           }

        
        

        }
        // else if (col.CompareTag("Player2"))
        // {
        //     CheckIfNoPlayer();
        // }

        // if (col.CompareTag("Player1"))
          // {
          //     m_isOccupied = false;
          // }
          // else if (col.CompareTag("Player2"))
          // {
          //     m_isOccupied = false;
          // }
          
          // m_isOccupied = false;
         
           
          if (m_isSwitchMovingPlatform)
          {
              GameLoader.Instance.StartMovingPlatform();
          }
          
       

           
        //}
    }
    public string GetNodeStory()
    {
        return m_story;
    }

    public bool IsStartNodePlayer1()
    {
        return m_startNodePlayer1;
    }
    
    public bool IsStartNodePlayer2()
    {
        return m_startNodePlayer2;
    }

    public void ResetData()
    {
       m_currentOccupiedTag = "";
       m_isOccupied = false;
        if(m_startNodePlayer1)
        {
             m_isOccupied = true;
             m_currentOccupiedTag = "Player1";
        }
        
        if (m_startNodePlayer2)
        {
            m_currentOccupiedTag = "Player2";
            m_isOccupied = true;
        }

        
      
       
    }


    
    
    
   




}
