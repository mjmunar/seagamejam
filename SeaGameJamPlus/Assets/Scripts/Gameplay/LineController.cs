﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineController : MonoBehaviour
{
   public static LineController Instance;
    [SerializeField] private LineRenderer m_lineRenderer;
    [SerializeField, Range(0,1)] private float multiplier;
    [SerializeField] private float m_lineRendererSpeed = .2f;
    [SerializeField] private bool isPlayer1;
    private Vector3 midPoint;
    private Vector3 p1Offset;
    private Vector3 p2Offset;
    private AnimationCurve curve;
    


    private GameObject m_player1;
    private GameObject m_player2;
    private BoxCollider2D col;
    private bool isColliderEmpty = false;
    private bool isRendererUpdated = true;
    private int m_currentPos;
    private int m_newPos;
    

    private void Start()
    {
        Instance = this;
        m_player1 = CharacterManager.Instance.GetPlayer1();
        m_player2 = CharacterManager.Instance.GetPlayer2();
        m_currentPos = CheckDistance(CharacterManager.Instance.GetPlayerDistance());
        curve = new AnimationCurve();
        curve.AddKey(0.0f, 1.0f);
        curve.AddKey(0.25f, 1f);
        curve.AddKey(0.75f, 1f);
        curve.AddKey(1.0f, 1.0f);
    }


    // Update is called once per frame
    private void FixedUpdate()
    {
        DrawLine(m_player1.transform.position, m_player2.transform.position);
        if (isPlayer1)
        {
            m_lineRenderer.material.SetTextureOffset("_MainTex", Vector2.right * (Time.time * m_lineRendererSpeed));
        }
        else
        {
            m_lineRenderer.material.SetTextureOffset("_MainTex", Vector2.left * (Time.time * m_lineRendererSpeed));
        }
        
        // addColliderToLine();
    }
    
    private void DrawLine(Vector3 p_player1, Vector3 p_player2)
    {
        // m_lineRenderer.positionCount = 3;
        //
        // Vector3[] points = new Vector3[3];
        
        midPoint = (p_player1 / 2) + (p_player2 / 2);
        p1Offset = (p_player1 / 2) + (midPoint / 2);
        p1Offset = (p_player1 / 2) + (p1Offset / 2);
        p2Offset = (p_player2 / 2) + (midPoint / 2);
        p2Offset = (p_player2 / 2) + (p2Offset / 2);
        
        // if (isPlayer1)
        // {
        //     points[0] = p_player1;
        //     points[1] = new Vector2(p1Offset.x, p1Offset.y);
        //     points[2] = new Vector2(midPoint.x, midPoint.y);
        // }
        // else
        // {
        //     points[0] = new Vector2(midPoint.x, midPoint.y);
        //     points[1] = new Vector2(p2Offset.x, p2Offset.y);
        //     points[2] = p_player2;
        // }
        
        
        m_lineRenderer.positionCount = 4;
		      
        Vector3[] points = new Vector3[4];
        
        points[0] = p_player1;
        points[1] = new Vector2(p1Offset.x, p1Offset.y);
        points[2] = new Vector2(p2Offset.x, p2Offset.y);
        points[3] = p_player2;
        
        
        
		
        m_lineRenderer.SetPositions(points);

        m_newPos = CheckDistance(CharacterManager.Instance.GetPlayerDistance());
        // if (m_newPos != m_currentPos)
        // {
        //     m_currentPos = m_newPos;
        //     if (isPlayer1)
        //     {
        //         StartCoroutine(SetCurvePlayer1(m_currentPos));
        //     }
        //     else
        //     {
        //         StartCoroutine(SetCurvePlayer2(m_currentPos));
        //     }
        //    
        // }
        
        
        m_lineRenderer.widthMultiplier = multiplier;
    }

    private int CheckDistance(float p_dist)
    {
        if (p_dist > CharacterManager.Instance.minDist && p_dist < CharacterManager.Instance.minMidPoint)
        {
            return 0;
        }
        else if (p_dist > CharacterManager.Instance.minMidPoint && p_dist < CharacterManager.Instance.midDist)
        {
            return 1;
        }
        else if (p_dist > CharacterManager.Instance.midDist && p_dist < CharacterManager.Instance.maxMidPoint)
        {
            return 2;
        }
        else if (p_dist > CharacterManager.Instance.maxMidPoint && p_dist < CharacterManager.Instance.maxDist)
        {
            return 3;
        }
        else if (p_dist > CharacterManager.Instance.maxMidPoint)
        {
            return 4;
        }

        return 0;
    }

    private IEnumerator SetCurvePlayer1(int p_value)
    {
            AnimationCurve newCurve = new AnimationCurve();
            float time = 1.5f;
            if (p_value.Equals(0))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value,1f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value,1f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value,1f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value,1f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
                
            }
            else if (p_value.Equals(1))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, 1f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, .75f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value, .75f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, .75f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
            }
            else if (p_value.Equals(2))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, 1f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, .5f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value, .5f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, .5f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
            }
            else if (p_value.Equals(3))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, 1f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, .25f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value, .25f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, .25f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
            }
            else if (p_value.Equals(4))
            {
                time = .5f;
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, 0f, .5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, 0f, .5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[2].value, 0f, .5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, 0f, .5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                EndDrawLine();
                yield return null;
            }
            yield return null;
    }
    
    private IEnumerator SetCurvePlayer2(int p_value)
    {
            AnimationCurve newCurve = new AnimationCurve();
            float time = 1.5f;
            if (p_value.Equals(0))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value,1f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value,1f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value,1f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value,1f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
                
            }
            else if (p_value.Equals(1))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, .75f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, .75f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value, .75f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, 1f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
            }
            else if (p_value.Equals(2))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, .5f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, .5f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value, .5f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, 1f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
            }
            else if (p_value.Equals(3))
            {
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, .25f, 1.5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, .25f, 1.5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[1].value, .25f, 1.5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, 1f, 1.5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                yield return null;
            }
            else if (p_value.Equals(4))
            {
                time = .5f;
                while (time > 0)
                {
                    curve.MoveKey(0, new Keyframe(0.0f, Mathf.Lerp(curve.keys[0].value, 0f, .5f)));
                    curve.MoveKey(1, new Keyframe(0.25f, Mathf.Lerp(curve.keys[1].value, 0f, .5f)));
                    curve.MoveKey(2, new Keyframe(0.75f, Mathf.Lerp(curve.keys[2].value, 0f, .5f)));
                    curve.MoveKey(3, new Keyframe(1.0f, Mathf.Lerp(curve.keys[3].value, 0f, .5f)));
                    time -= Time.fixedDeltaTime;
                    m_lineRenderer.widthCurve = curve;
                }
                EndDrawLine();
                yield return null;
            }
            yield return null;
    }
    
    public  void EndDrawLine()
    {
        m_lineRenderer.positionCount = 0;
    }
    
    // Following method adds collider to created line
    // private void addColliderToLine()
    // {
    //     if (!isColliderEmpty)
    //     {
    //         col = new GameObject("Collider").AddComponent<BoxCollider2D> ();
    //         col.gameObject.AddComponent<Rigidbody2D>();
    //         col.transform.parent = m_lineRenderer.transform; // Collider is added as child object of line
    //         isColliderEmpty = true;
    //     }
    //     
    //     float lineLength = Vector3.Distance (m_player1.transform.position, m_player2.transform.position); // length of line
    //     col.size = new Vector3 (lineLength, 0.1f, 1f); // size of collider is set where X is length of line, Y is width of line, Z will be set as per requirement
    //     Vector3 midPoint = (m_player1.transform.position + m_player2.transform.position)/2;
    //     col.transform.position = midPoint; // setting position of collider object
    //     // Following lines calculate the angle between startPos and endPos
    //     float angle = (Mathf.Abs (m_player1.transform.position.y - m_player2.transform.position.y) / Mathf.Abs (m_player1.transform.position.x - m_player2.transform.position.x));
    //     if((m_player1.transform.position.y<m_player2.transform.position.y && m_player1.transform.position.x>m_player2.transform.position.x) || (m_player2.transform.position.y<m_player1.transform.position.y && m_player2.transform.position.x>m_player1.transform.position.x))
    //     {
    //         angle*=-1;
    //     }
    //     angle = Mathf.Rad2Deg * Mathf.Atan (angle);
    //     col.transform.Rotate (0, 0, angle);
    // }

    // private void OnDrawGizmos()
    // {
    //     if (isPlayer1)
    //     {
    //         Gizmos.DrawCube(p1Offset, new Vector3(.2f,.2f,.2f));
    //     }
    //
    //     else
    //     {
    //         Gizmos.DrawCube(p2Offset, new Vector3(.2f,.2f,.2f));
    //     }
    //
    //     Gizmos.DrawSphere(midPoint, .3f);
    //     
    // }
}
