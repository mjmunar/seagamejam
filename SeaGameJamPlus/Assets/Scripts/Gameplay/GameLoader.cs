﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    public static GameLoader Instance;
    [SerializeField] private int m_currentLevel = 1;
    public int m_nextLevelGoal = 0;

    public bool m_resetting = false;

    [SerializeField] SpriteRenderer m_fadeSprite;
    [SerializeField] SpriteRenderer m_nextLevelSprite;
    
    [SerializeField] float  m_durations;
    
    void Start()
    {
        Instance = this;


        //ResetLevel();
       // SetPlayerStartPosition();
       // SetPlayerStartPosition();
   
      SetActiveNextLevel();



    }

    [SerializeField] private NodeController[] m_parentNodeLevel;


    public void SetPlayerStartPosition()
    {
        
        CharacterManager.Instance.ResetPlayerData();
      //  CharacterManager.Instance.GetPlayerController("Player1").InitializeMoveTowardsNode(GetPlayerStartPosition(0));
      // CharacterManager.Instance.GetPlayerController("Player2").InitializeMoveTowardsNode(GetPlayerStartPosition(1));
       CharacterManager.Instance.GetPlayerController("Player1").transform.position = GetPlayerStartPosition(0);
       CharacterManager.Instance.GetPlayerController("Player2").transform.position = GetPlayerStartPosition(1);
      


    }
    

    public Vector2 GetPlayerStartPosition(int p_value)
    {
        return m_parentNodeLevel[m_currentLevel - 1].GetStartNodePlayer(p_value);
    }
    
    public void ResetLevel(int p_value = 1)
    {
        m_resetting = true;
        StartCoroutine(ResetLevelMechanism(p_value));
    }

    IEnumerator ResetLevelMechanism(int p_value = 1)
    {
       
      
      
       
        if (p_value == 0)
        {
            //KAKIMA ADD FADE
            yield return new WaitForSeconds(1);
            Fade.Instance.FadeOut(m_durations,0,m_fadeSprite);
          
        }

       // Time.timeScale = 1;
        CharacterManager.Instance.SetObjectsActive(false);
        m_parentNodeLevel[m_currentLevel - 1].ResetNodeData();
        SetPlayerStartPosition();
        m_nextLevelGoal = 0;
       
        StartCoroutine(DelayReset(m_durations));
        
        //to add reset position of players
      //  Time.timeScale = 1;
     
      
    }
    
    
    
    public void SetActiveNextLevel()
    {
        //next level
        if (DialogueController.Instance != null)
        {
            DialogueController.Instance.DisplayDialogue();
        }
        Fade.Instance.FadeOut(m_durations + 2,0,m_fadeSprite);
        m_parentNodeLevel[m_currentLevel - 1].gameObject.SetActive(true);
        if(SoundManager.Instance != null)
        SoundManager.Instance.PlayBGM(m_currentLevel);
        ResetLevel();
        //disable previous level
        if (m_currentLevel != 1)
        {
            m_parentNodeLevel[m_currentLevel - 2].gameObject.SetActive(false);
        }
    }

    public void StartMovingPlatform()
    {
        m_parentNodeLevel[m_currentLevel - 1].StartMovingPlatform();
    }
    
    public void StopMovingPlatform()
    {
        m_parentNodeLevel[m_currentLevel - 1].StopMovingPlatform();
    }
    
    public void OpenDoor(bool p_value)
    {
        m_parentNodeLevel[m_currentLevel - 1].OpenDoor(p_value);
    }
    

    public void AddGoalCounter()
    {
        m_nextLevelGoal++;
        if (m_nextLevelGoal >= 2)
        {
            m_currentLevel++;
            if (m_currentLevel > 6)
            {
                //KAKIMA change scene victory ek ek
                //victory scene
                DialogueController.Instance.DisplayDialogue();
                DialogueController.Instance.OpenEndScene();
            }
            else
            {
                //KAKIMA next level scene (loading)
                SetActiveNextLevel();
                Debug.Log("Next Level");
                m_nextLevelGoal = 0;
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            GameLoader.Instance.ResetLevel(0);
        }
        
        if (Input.GetKeyDown(KeyCode.T))
        {
            Time.timeScale = 1;
        }
                

    }

    IEnumerator DelayReset(float p_value)
    {
        yield return new WaitForSeconds(p_value);
        m_resetting = false;
        CharacterManager.Instance.SetObjectsActive(true);

    }

    public bool IsGameLoaderResetting()
    {
        return m_resetting;
    }
    
    public void SetResetting()
    {
        m_resetting = true;
    }

   

}


