﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class NodeController : MonoBehaviour
{
  
    
    [SerializeField] private NodeItem[] m_nodesLevel;
    [SerializeField] private BlockerObject[] m_movingBlockerObjectsLevel;

    [SerializeField] private GameObject m_singleBlocker;
    
    public void CheckNodeStory()
    {
     
    }
    public void StartMovingPlatform()
    {
        if (m_movingBlockerObjectsLevel != null)
        {
            for (int i = 0; i < m_movingBlockerObjectsLevel.Length; i++)
            {
                m_movingBlockerObjectsLevel[i].StartMoving();
            }
        }
    }
    
    public void StopMovingPlatform()
    {
        if (m_movingBlockerObjectsLevel != null)
        {
            for (int i = 0; i < m_movingBlockerObjectsLevel.Length; i++)
            {
                m_movingBlockerObjectsLevel[i].StopMoving();
            }
            
        }
    }
    public void OpenDoor(bool p_value)
    {
        if (m_singleBlocker != null)
        {
            m_singleBlocker.gameObject.SetActive(p_value);
        }
    }
    
    public void ResetBlockers()
    {
        if (m_movingBlockerObjectsLevel != null)
        {
            for (int i = 0; i < m_movingBlockerObjectsLevel.Length; i++)
            {
                m_movingBlockerObjectsLevel[i].StartMoving();
            }
        }
        
        if (m_singleBlocker != null)
        {
            m_singleBlocker.gameObject.SetActive(true);
        }
    }
    
    public Vector2 GetStartNodePlayer(int p_value)
    {
        if (p_value == 0)
        {
            for (int i = 0; i < m_nodesLevel.Length; i++)
            {
                if (m_nodesLevel[i].IsStartNodePlayer1())
                {
                    return m_nodesLevel[i].transform.position;
                }
            }
        }
        
        else if(p_value == 1)
        {
            for (int i = 0; i < m_nodesLevel.Length; i++)
            {
                if (m_nodesLevel[i].IsStartNodePlayer2())
                {
                    return m_nodesLevel[i].transform.position;
                }
            }
                 
        }

        return Vector2.zero;
    }

    public void ResetNodeData()
    {
        for (int i = 0; i < m_nodesLevel.Length; i++)
        {
            m_nodesLevel[i].SetActiveGameObject(true);
            // if (i == 0 || i == 1)
            // {
            //     m_nodesLevel[i].SetStarNodesOccupied();
            // }
            // else
            // {
                m_nodesLevel[i].ResetData();
           // }
        }

        ResetBlockers();
    }
    
    
        

}
