﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    public AudioClip lobbyBGM;
    public AudioClip level1BGM;
    public AudioClip level2BGM;
    public AudioClip level3BGM;
    public AudioClip level4BGM;
    public AudioClip level5BGM;

    public AudioClip[] m_sfx;

    [SerializeField] private AudioSource m_audioSource;
    [SerializeField] private AudioClip lastAudioClip;
    
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        Instance = this;
        m_audioSource.clip = lobbyBGM;
        m_audioSource.Play();
    }

    public void PlayBGM(int p_level)
    {
        Debug.Log(p_level);
        
        if(p_level == 2) return;
        
        m_audioSource.Stop();
        
        switch (p_level)
        {
            case 1:
                m_audioSource.clip = level1BGM;
                break;
            case 3:
                m_audioSource.clip = level2BGM;
                break;
            case 4:
                m_audioSource.clip = level3BGM;
                break;
            case 5:
                m_audioSource.clip = level4BGM;
                break;
            case 6:
                m_audioSource.clip = level5BGM;
                break;
        }
            
            m_audioSource.Play();
        
    }
}
