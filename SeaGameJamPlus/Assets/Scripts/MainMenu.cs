﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
   [SerializeField] private GameObject m_title;
   [SerializeField] private GameObject m_nodeObject;
   [SerializeField] private Collider2D m_characterCollider;


   private void Update()
   {

      if (Input.GetMouseButtonDown(0))
      {
         m_title.SetActive(false);
         m_nodeObject.SetActive(true);
         m_characterCollider.enabled = true;
      }

   }
}
